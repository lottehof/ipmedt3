
window.onload = () => {
  const PLACES = document.getElementsByClassName('js--places');
  const HANDMODELPLACEHOLDER = document.getElementById('js--handModelPlaceholder');
  const LOOKMODEL = document.getElementById('js--lookmodel');
  const BUTTONPLACEHOLDER = document.getElementById('js--buttonPlaceholder');
  const CHECK = document.getElementById("js--checkButton");
  const CURSOR = document.getElementById("js--cursor");
  const OPNIEUW = document.getElementById("js--replay");
  const GOED = document.getElementById('js--good');
  const CAMERACONTAINER = document.getElementById("js--camera-container");
  const NAMEDIALOGRETRY = document.getElementById('js--nameDialogRetry');
  const NAMEDIALOGNEXT = document.getElementById('js--nameDialogNext');
  const CAMERA = document.getElementById('js--camera-container');
  const NAME = document.getElementById('js--Name');
  const BLACKBOARD = document.getElementById('js--blackboard');
  const WELCOMEDIALOGNEXT = document.getElementById('js--welcomeDialogNext')
  const WELCOMEDIALOG = document.getElementById('js--welcomeDialog')
  const NAMEDIALOG = document.getElementById('js--nameDialog');
  var startDetecting = false;
  var enableMoving = true;
  var juistGebaar = null;
  var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
  var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent
  var recognition = new SpeechRecognition();
  var naam = null;
  recognition.lang = 'nl-NL';
  var FnotDetected = true;
  WELCOMEDIALOGNEXT.addEventListener('click', function(evt){
    WELCOMEDIALOG.setAttribute('visible', 'false')
    NAMEDIALOG.setAttribute('visible', 'true')
    WELCOMEDIALOGNEXT.parentElement.removeChild(WELCOMEDIALOGNEXT)
    startSpeechRecognition()
  })

  for(let i = 0;i < PLACES.length;i++){
      PLACES[i].addEventListener('click', function(evt){
          let att = document.createAttribute("animation");
          att.value = "property: position; easing: linear; dur: 2000; to: " +
          this.getAttribute("position").x + " 1.6 " + this.getAttribute("position").z;
          CAMERA.setAttribute("animation", att.value);
          CURSOR.setAttribute('material', {color: "white"});



      });
  }

  OPNIEUW.addEventListener('click', function(evt){
    HANDMODELPLACEHOLDER.emit('startHand')
    HANDMODELPLACEHOLDER.children[0].emit('startHand')
  })

  Leap.loop(
      { background: true },
      {
        hand: function(hand) {
            if(!hand.valid){
              console.log("No hand")

            }
          if (hand.valid) {
              document.getElementById('js--handNotDetected').setAttribute('visible', 'false')
            if (hand.pointables[1].tipPosition[0] > 0.20 && enableMoving){
              let currentRotation = CAMERACONTAINER.getAttribute('rotation')
              let newDirection = currentRotation.x + " " + (currentRotation.y - 0.5) + " " + currentRotation.z
              CAMERACONTAINER.setAttribute("rotation", newDirection);
            }
            if (hand.pointables[1].tipPosition[0] < -0.20 && enableMoving) {
              let currentRotation = CAMERACONTAINER.getAttribute('rotation')
              let newDirection = currentRotation.x + " " + (currentRotation.y + 0.5) + " " + currentRotation.z
              CAMERACONTAINER.setAttribute("rotation", newDirection);
            }
            if (hand.pointables[1].tipPosition[1] < -0.10 && enableMoving) {
              let currentRotation = CAMERACONTAINER.getAttribute('rotation')
              let newDirection = (currentRotation.x - 0.3) + " " + currentRotation.y + " " + currentRotation.z
              CAMERACONTAINER.setAttribute("rotation", newDirection);
            }
            if (hand.pointables[1].tipPosition[1] > 0.16 & enableMoving) {
              let currentRotation = CAMERACONTAINER.getAttribute('rotation')
              let newDirection = (currentRotation.x + 0.3) + " " + currentRotation.y + " " + currentRotation.z
              CAMERACONTAINER.setAttribute("rotation", newDirection);
            }

            //INDEX FINGER
            var dotIndexFingerProximalMedial = Leap.vec3.dot(
              hand.indexFinger.proximal.direction(), //proxima = van eerst knobbel naar tweede knobbel
              hand.indexFinger.medial.direction() // medial =  van tweede knobbel naar derde knobbel
            );
            var dotIndexFingerMetacarpalProximal = Leap.vec3.dot(
              hand.indexFinger.metacarpal.direction(), //pols naar vinger
              hand.indexFinger.proximal.direction()
            );
            var dotIndexFingerMedialDistal = Leap.vec3.dot(
              hand.indexFinger.medial.direction(),
              hand.indexFinger.distal.direction()
            )
            var dotIndexDistal = hand.indexFinger.distal.direction()

            var dotIndexMedial = hand.indexFinger.medial.direction()
            //----------------------------------------------------
            //MIDDLE FINGER
            var dotMiddleFingerProximalMedial = Leap.vec3.dot(
              hand.middleFinger.proximal.direction(),
              hand.middleFinger.medial.direction()
            );
            var dotMiddleFingerProximal = hand.middleFinger.proximal.direction()

            var dotMiddleFingerMetacarpalProximal = Leap.vec3.dot(
              hand.middleFinger.metacarpal.direction(),
              hand.middleFinger.proximal.direction()
            );
            var dotMiddleFingerDistal = hand.middleFinger.distal.direction()
            //RING FINGER
            var dotRingFingerProximalMedial = Leap.vec3.dot(
              hand.ringFinger.proximal.direction(),
              hand.ringFinger.medial.direction()
            );

            var dotRingFingerMetacarpalProximal = Leap.vec3.dot(
              hand.ringFinger.metacarpal.direction(),
              hand.ringFinger.proximal.direction()
            );
            var dotRingFingerDistal = hand.ringFinger.distal.direction()
            //----------------------------------------------------
            //PINKY FINGER
            var dotPinkyProximalMedial = Leap.vec3.dot(
              hand.pinky.proximal.direction(),
              hand.pinky.medial.direction()
            );

            var dotPinkyMetacarpalProximal = Leap.vec3.dot(
              hand.pinky.metacarpal.direction(),
              hand.pinky.proximal.direction()
            );
            var dotPinkyDistal = hand.pinky.distal.direction()
            //----------------------------------------------------
            //Thumb
            var dotThumbProximalMedial = Leap.vec3.dot(
              hand.thumb.proximal.direction(),
              hand.thumb.medial.direction()
            )
            var dotThumbMedialDistal = Leap.vec3.dot(
              hand.thumb.medial.direction(),
              hand.thumb.distal.direction()
            )

            var dotThumbDistal = hand.thumb.distal.direction()
            //----------------------------------------------------
            //ANGLE INDEX FINGER
            var angleIndexFingerProximalMedial = Math.acos(
              dotIndexFingerProximalMedial
            );
            var angleIndexFingerMetacarpalProximal = Math.acos(
              dotIndexFingerMetacarpalProximal
            );
            var angleIndexFingerMedialDistal = Math.acos(dotIndexFingerMedialDistal);

            //----------------------------------------------------
            // ANGLE MIDDLE FINGER
            var angleMiddleFingerProximalMedial = Math.acos(
              dotMiddleFingerProximalMedial
            );
            var angleMiddleFingerMetacarpalProximal = Math.acos(
              dotMiddleFingerMetacarpalProximal
            );

            //----------------------------------------------------
            // ANGLE RING FINGER
            var angleRingFingerProximalMedial = Math.acos(
              dotRingFingerProximalMedial
            );
            var angleRingFingerMetacarpalProximal = Math.acos(
              dotRingFingerMetacarpalProximal
            );
            //----------------------------------------------------
            // ANGLE PINKY FINGER
            var anglePinkyProximalMedial = Math.acos(dotPinkyProximalMedial);
            var anglePinkyMetacarpalProximal = Math.acos(
              dotPinkyMetacarpalProximal
            );
            //----------------------------------------------------
            // ANGLE THUMB FINGER
            var angleThumbProximalMedial = Math.acos(dotThumbProximalMedial);
            var angleThumbMedialDistal = Math.acos(dotThumbMedialDistal);

            //----------------------------------------------------
            // DISTAL THUMB INDEX DISTANCE
            var dotThumbIndexDistalDistance = dotThumbDistal[1] - dotIndexDistal[1];
            //----------------------------------------------------
            //DISTAL THUMB MIDDLEFINGER DISTANCE
            var dotThumbMiddleDistalDistance = dotThumbDistal[1] - dotMiddleFingerDistal[1];
            var dotIndexMiddleFingerDistalDistance = dotIndexDistal[1] - dotMiddleFingerDistal[1];
            //-------------------------------------------------------
            //DISTAL MIDDLE RING DISTANCE
            var dotMiddleRingDistalDistance = dotMiddleFingerDistal[1]- dotRingFingerDistal[1];

            //--------------------------------------------------------
            // DISTAL RING PINKY DISTANCE
            var dotRingPinkyDistalDistance = dotRingFingerDistal[1] - dotPinkyDistal[1];

            //------------------------------------------------------
            // THUMB DISTAL AND INDEX MEDIAL DISTANCE
            var dotThumbDistalIndexMedial = dotThumbDistal[1] - dotIndexMedial[1];

            //------------------------------------------------------
            // THUMB DISTAL MIDDLEFINGER PROXIMAL DISTANCE
            var dotThumbMiddleFingerDistalProximalDistance = dotThumbDistal[1] - dotMiddleFingerProximal[1];


           if ( //extended betekent dat vingers niet rechtstaan (handgebaar van A)
              !hand.indexFinger.extended &&
              !hand.middleFinger.extended &&
              !hand.ringFinger.extended &&
              !hand.pinky.extended &&
              hand.thumb.extended &&
              startDetecting
            ) {
               checkGebaar("A")
            } else if (
              hand.indexFinger.extended &&
              hand.middleFinger.extended &&
              hand.ringFinger.extended &&
              hand.pinky.extended &&
              !hand.thumb.extended &&
              startDetecting
            ) {
              checkGebaar("B")
            } else if (
              angleIndexFingerMetacarpalProximal < 0.5 &&
              angleIndexFingerProximalMedial > 0.9 &&
              angleIndexFingerProximalMedial < 1.1 &&
              angleMiddleFingerMetacarpalProximal < 0.5 &&
              angleMiddleFingerProximalMedial > 0.9 &&
              angleMiddleFingerProximalMedial < 1.5 &&
              angleRingFingerMetacarpalProximal < 0.5 &&
              angleRingFingerProximalMedial > 0.75 &&
              angleRingFingerProximalMedial < 1.5 &&
              anglePinkyMetacarpalProximal < 0.5 &&
              anglePinkyProximalMedial > 0.75 &&
              anglePinkyProximalMedial < 1.5 &&
              startDetecting
            ) {
              checkGebaar("C")
            } else if (
              hand.indexFinger.extended &&
              !hand.middleFinger.extended &&
              !hand.ringFinger.extended &&
              !hand.pinky.extended &&
              !hand.thumb.extended &&
              startDetecting
            ) {
              checkGebaar("D")
            } else if (
              !hand.indexFinger.extended &&
              !hand.middleFinger.extended &&
              !hand.ringFinger.extended &&
              !hand.pinky.extended &&
              !hand.thumb.extended &&
              startDetecting
            )
            { if (
              dotThumbIndexDistalDistance > 0.9 &&
              dotThumbIndexDistalDistance < 1.2 &&
              dotIndexFingerMetacarpalProximal > 0.5 &&
              dotIndexFingerMetacarpalProximal < 0.75
            ){
              checkGebaar("G")
            } else if(  dotMiddleRingDistalDistance <= 1 &&
                        dotMiddleRingDistalDistance >=0.05
                      ){
                         checkGebaar("S")
                       } else {
                    checkGebaar("E")
                  }
            }   else if(
              hand.middleFinger.extended &&
              hand.ringFinger.extended &&
              hand.pinky.extended

            )
             { if (
                  dotThumbIndexDistalDistance > 1.23 &&
                  dotThumbIndexDistalDistance < 1.33

              ){
                checkGebaar("F");
                      } else if (
                        dotThumbIndexDistalDistance > 1.18 &&
                        dotThumbIndexDistalDistance < 1.229
                      ){
                        checkGebaar("T");
                      }
                  }

           else if(
              dotIndexMiddleFingerDistalDistance < 0 &&
              dotIndexMiddleFingerDistalDistance >= -0.2 &&
              hand.indexFinger.extended &&
              hand.middleFinger.extended &&
              !hand.ringFinger.extended &&
              !hand.pinky.extended &&
              !hand.thumb.extended &&
              startDetecting
          ){
            checkGebaar("H") //werkt NO MOTION CHECK
          } else if(
            !hand.indexFinger.extended &&
            !hand.middleFinger.extended &&
            !hand.ringFinger.extended &&
            hand.pinky.extended &&
            !hand.thumb.extended &&
            startDetecting
          ){
            checkGebaar("I")
          } else if(
            !hand.indexFinger.extended &&
            !hand.middleFinger.extended &&
            !hand.ringFinger.extended &&
            hand.pinky.extended &&
            !hand.thumb.extended&&
            startDetecting
        ){
          checkGebaar("J") //werkt NO MOTION CHECK
        }  else if(
          dotIndexMiddleFingerDistalDistance <= 0.2 &&
          dotIndexMiddleFingerDistalDistance >= -0.70 &&

          dotThumbMiddleFingerDistalProximalDistance <= 0 &&
          dotThumbMiddleFingerDistalProximalDistance >= -0.70 &&
          !hand.thumb.extended &&
          hand.indexFinger.extended &&
          hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          startDetecting
        ){
          checkGebaar("K");
        }else if (
          dotThumbIndexDistalDistance >= -0.85 &&
          dotThumbIndexDistalDistance <= -0.40 &&
          hand.indexFinger.extended &&
          !hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          hand.thumb.extended &&
          startDetecting
       ){
         checkGebaar("L")
       }
       else if(
         dotMiddleRingDistalDistance < 0 &&
         dotMiddleRingDistalDistance > -1 &&
         angleIndexFingerMetacarpalProximal > 0.40 &&
         angleIndexFingerMetacarpalProximal < 1.1 &&
         angleMiddleFingerMetacarpalProximal < 0.8 &&
         angleMiddleFingerMetacarpalProximal > 0.4 &&
         angleRingFingerMetacarpalProximal < 1.2 &&
         angleRingFingerMetacarpalProximal > 0.60 &&
         !hand.thumb.extended &&
         !hand.pinky.extended &&
         !hand.ringFinger.extended &&
         !hand.indexFinger.extended &&
         !hand.middleFinger.extended &&
         startDetecting
       ){ if(
            dotIndexMiddleFingerDistalDistance < 0.1 &&
            dotIndexMiddleFingerDistalDistance > -0.5
          ){
            checkGebaar("N");
          } else {
         checkGebaar("M");
          }
       }
         else if(
         !hand.thumb.extended &&
         !hand.indexFinger.extended &&
         !hand.middleFinger.extended &&
         !hand.ringFinger.extended &&
         !hand.pinky.extended &&
         startDetecting
        ){
          checkGebaar("O");
        }
      else if(
        dotThumbIndexDistalDistance < -0.05 &&
        dotThumbIndexDistalDistance > -0.70 &&


        !hand.thumb.extended &&
        !hand.pinky.extended &&
        !hand.ringFinger.extended &&
        hand.indexFinger.extended &&
        !hand.middleFinger.extended &&
        startDetecting
      ){
        checkGebaar("P");
     }
    else if(
         dotThumbIndexDistalDistance > 1.20 &&
         dotThumbIndexDistalDistance < 1.40 &&
         !hand.thumb.extended &&
         !hand.indexFinger.extended &&
         !hand.middleFinger.extended &&
         !hand.ringFinger.extended &&
         !hand.pinky.extended &&
         startDetecting
       ){
         checkGebaar("Q");
      } else if(
          dotThumbIndexDistalDistance > 0.6 &&
          dotThumbIndexDistalDistance < 0.9 &&
          dotThumbMiddleDistalDistance > 0.7 &&
          dotThumbMiddleDistalDistance < 1 &&
          dotIndexMiddleFingerDistalDistance > 0 &&
          dotIndexMiddleFingerDistalDistance < 0.3 &&

          !hand.thumb.extended &&
          !hand.indexFinger.extended &&
          !hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          startDetecting
        ){
          checkGebaar("R");
      } else if (
          dotIndexMiddleFingerDistalDistance < 0 &&
          dotIndexMiddleFingerDistalDistance >= -1 &&
          hand.indexFinger.extended &&
          hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          !hand.thumb.extended &&
          startDetecting
        ){
          checkGebaar("U") //gecommit en werkt NO MOTION CHECK
        }
        else if(
          dotIndexMiddleFingerDistalDistance < 1 &&
          dotIndexMiddleFingerDistalDistance > 0 &&
          hand.indexFinger.extended &&
          hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          !hand.thumb.extended &&
          startDetecting
        ){
          checkGebaar("V") //gecommit en werkt
        } else if(
          dotThumbIndexDistalDistance > -0.80 &&
          dotThumbIndexDistalDistance < - 0.50 &&

          hand.indexFinger.extended &&
          hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          hand.thumb.extended &&
          startDetecting
        ){
          checkGebaar("W") //gecommit en werkt
        }
        else if(
          angleIndexFingerProximalMedial < 1.14 &&
          angleIndexFingerProximalMedial > 1.12 &&

          angleIndexFingerMedialDistal < 0.92 &&
          angleIndexFingerMedialDistal > 0.90 &&

          angleThumbProximalMedial <=0.16 &&
          angleThumbProximalMedial >= 0.13 &&

          angleThumbMedialDistal <= 0.30 &&
          angleThumbMedialDistal >= 0.27 &&

          !hand.pinky.extended &&
          !hand.ringFinger.extended &&
          !hand.middleFinger.extended &&
          startDetecting

        ){
          checkGebaar("X");
        }
        else if(
          hand.thumb.extended &&
          hand.pinky.extended &&
          !hand.indexFinger.extended &&
          !hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          startDetecting
        ){
          checkGebaar("Y") //gecommit en werkt
        }
        else if (
          hand.indexFinger.extended &&
          !hand.middleFinger.extended &&
          !hand.ringFinger.extended &&
          !hand.pinky.extended &&
          !hand.thumb.extended &&
          startDetecting
        ) {
          checkGebaar("Z") //werkt NO MOTION CHECK
        }
      }
    }
    })

    function selectLetter (letter) {
      juistGebaar = letter
      LOOKMODEL.setAttribute('visible', true)
      let hand = document.createElement('a-entity')
      hand.setAttribute('gltf-model', "#" + letter)
      hand.setAttribute('id', 'js--handModel')
      hand.setAttribute('shadow', 'receive: false')
      if (letter == 'H'){
        hand.setAttribute('animation__2', 'property: position; from: 0 0 -2; to: 0 0 0; dur: 1000; easing: linear; startEvents: startHand;')
      }
      else if (letter == 'J'){
        hand.setAttribute('animation__2', 'property: rotation; to: -90 0 0; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__3', 'property: rotation; to: -90 0 -90; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__4', 'property: position; from: 0 0 0; to: 0 0 1; delay: 2000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__5', 'property: position; from: 0 0 1; to: -1 0 2; delay: 3000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__6', 'property: position; from: -1 0 2; to: -1.5 0 2; delay: 4000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__7', 'property: position; from: -1.5 0 2; to: -2 0 1; delay: 5000; dur: 1000; easing: linear; startEvents: startHand;')

      }
      else if (letter == 'U'){
        hand.setAttribute('animation__2', 'property: rotation; to: -90 0 0; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__3', 'property: position; from: 0 0 0; to: 0 0 1; delay: 1000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__4', 'property: position; from: 0 0 1; to: 1 0 2; delay: 2000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__5', 'property: position; from: 1 0 2; to: 2 0 2; delay: 3000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__6', 'property: position; from: 2 0 2; to: 3 0 1; delay: 4000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__7', 'property: position; from: 3 0 1; to: 3 0 -1; delay: 5000; dur: 1000; easing: linear; startEvents: startHand;')
      }
      else if(letter == "Z"){
        hand.setAttribute('animation__2', 'property: rotation; to: -90 0 0; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__3', 'property: position; from: 0 0 0; to: -1 0 0; delay: 1000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__4', 'property: position; from: -1 0 0; to: 0 0 2; delay: 2000; dur: 1000; easing: linear; startEvents: startHand;')
        hand.setAttribute('animation__5', 'property: position; from: 0 0 2; to: -1 0 2; delay: 3000; dur: 1000; easing: linear; startEvents: startHand;')
      }
      else if(letter == "X"){
        hand.setAttribute('animation__2', 'property: rotation; to: -90 0 0; delay:1000; dur: 1000; easing: linear; startEvents: startHand;')
      }
      HANDMODELPLACEHOLDER.appendChild(hand)
      for(let i = 0;i < PLACES.length;i++){
        PLACES[i].setAttribute('visible', true)
        PLACES[i].classList.add('clickable')
      }
      BUTTONPLACEHOLDER.setAttribute('visible', 'true')
      CHECK.classList.add('clickable')
      OPNIEUW.classList.add('clickable')
      CHECK.setAttribute('visible', 'true')
      OPNIEUW.setAttribute('visible', 'true')
      let att = document.createAttribute("animation");
      att.value = "property: position; easing: linear; dur: 2000; to: " +
      "0" + " 1.6 " + "-30";
      let blackboardLetters =  BLACKBOARD.children
      for(let i = 1; i < blackboardLetters.length; i++){
          blackboardLetters[i].classList.remove('clickable')
      }
      setTimeout(function(){
        HANDMODELPLACEHOLDER.emit('startHand');
        hand.emit('startHand');
      }, 2000)
      CAMERA.setAttribute("animation", att.value);
      CHECK.addEventListener('click', function(){
        startDetecting = true
        enableMoving = false
        CHECK.setAttribute('visible', 'false')
        OPNIEUW.setAttribute('visible', 'false')
        CHECK.classList.remove('clickable')
        OPNIEUW.classList.remove('clickable')
        let hand = document.getElementById('js--handModel');
        hand.parentNode.removeChild(hand);
        console.log(juistGebaar)
      })
    }
    function startSpeechRecognition() {
      recognition.start();
      NAMEDIALOGNEXT.setAttribute('visible', 'false')
      NAMEDIALOGRETRY.setAttribute('visible', 'false')
      recognition.onresult = function(evt) {
        var last = evt.results.length -1;
        naam = evt.results[last][0].transcript;
        NAME.setAttribute('value', naam)
      }
      recognition.onspeechend = function() {
        recognition.stop()
        NAMEDIALOGNEXT.setAttribute('visible', 'true')
        NAMEDIALOGRETRY.setAttribute('visible', 'true')
        NAMEDIALOGRETRY.classList.add('clickable')
        NAMEDIALOGNEXT.classList.add('clickable')
        NAMEDIALOGRETRY.addEventListener('click', function (evt){
          NAMEDIALOGNEXT.classList.remove('clickable')
          NAMEDIALOGRETRY.classList.remove('clickable')
          startSpeechRecognition();
        })
        NAMEDIALOGNEXT.addEventListener('click', function (evt){
          document.getElementById('js--frontSpot').setAttribute('visible', 'true')
          for (let i = 0; i < naam.length; i++){
            let letter = naam.charAt(i)
            let letterElement = document.createElement('a-text')
            letterElement.setAttribute('dynamic-body', '')
            letterElement.setAttribute('id', 'js--' + letter.toUpperCase())
            letterElement.setAttribute('class', 'clickable')
            letterElement.setAttribute('width', '150px')
            letterElement.setAttribute('color', 'white')
            letterElement.setAttribute('value', letter.toUpperCase())
            letterElement.setAttribute('align', 'center')
            BLACKBOARD.appendChild(letterElement)
            letterElement.setAttribute('position', {x: ((3 - naam.length / 2 * 7) + (i * 7)), y: 0, z: 1})
            letterElement.setAttribute('geometry', {primitive: 'plane', width: 7, height: 8.5})
            letterElement.setAttribute('material', {transparent: true, opacity: 0})
            letterElement.addEventListener('click', function (evt) {
              selectLetter(letter.toUpperCase())
            })
          }
          NAME.parentElement.removeChild(NAME)
          NAMEDIALOGNEXT.parentElement.removeChild(NAMEDIALOGNEXT)
          NAMEDIALOGRETRY.parentElement.removeChild(NAMEDIALOGRETRY)
          NAMEDIALOG.parentElement.removeChild(NAMEDIALOG)
        })

      }
      recognition.onnomatch = function(evt) {
        NAME.setAttribute('value', "Ik kan het niet verstaan")
      }
      recognition.onerror = function(evt) {
        console.log('Error in text-to-speech: ' + evt.error)
      }
    }
    function checkGebaar(gebaar){
      if (juistGebaar == gebaar){
        console.log(gebaar)
        GOED.setAttribute('visible', 'true')
        setTimeout(function(){
          startDetecting = false
          enableMoving = true
          let att = document.createAttribute("animation");
          att.value = "property: position; easing: linear; dur: 2000; to: " +
          "0" + " 1.6 " + "0";
          let blackboardLetters =  BLACKBOARD.children
          for(let i = 1; i < blackboardLetters.length; i++){
              blackboardLetters[i].classList.add('clickable')
          }
          CAMERA.setAttribute("animation", att.value);
          for(let i = 0;i < PLACES.length;i++){
            PLACES[i].setAttribute('visible', false)
            PLACES[i].classList.remove('clickable')
          }
          GOED.setAttribute('visible', 'false')
        }, 3000)

    }
    }

}

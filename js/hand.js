    const CAMERACONTAINER = document.getElementById("js--camera-container");

Leap.loop(
    { background: true },
    {
      hand: function(hand) {
        if (hand) {
          console.log(hand.pointables[1].tipPosition)
          if (hand.pointables[1].tipPosition[0] > 0.30){
            let currentRotation = CAMERACONTAINER.getAttribute('rotation')
            let newDirection = currentRotation.x + " " + (currentRotation.y - 0.5) + " " + currentRotation.z
            CAMERACONTAINER.setAttribute("rotation", newDirection);
          }
          if (hand.pointables[1].tipPosition[0] < -0.20) {
            let currentRotation = CAMERACONTAINER.getAttribute('rotation')
            let newDirection = currentRotation.x + " " + (currentRotation.y + 0.5) + " " + currentRotation.z
            CAMERACONTAINER.setAttribute("rotation", newDirection);
          }
          if (hand.pointables[1].tipPosition[1] < -0.11) {
            let currentRotation = CAMERACONTAINER.getAttribute('rotation')
            let newDirection = (currentRotation.x - 0.5) + " " + currentRotation.y + " " + currentRotation.z
            CAMERACONTAINER.setAttribute("rotation", newDirection);
          }
          if (hand.pointables[1].tipPosition[1] > 0.20) {
            let currentRotation = CAMERACONTAINER.getAttribute('rotation')
            let newDirection = (currentRotation.x + 0.5) + " " + currentRotation.y + " " + currentRotation.z
            CAMERACONTAINER.setAttribute("rotation", newDirection);
          }
          // let direction =
          //   hand.pointables[1].tipPosition[0] +
          //   " " +
          //   hand.pointables[1].tipPosition[1] +
          //   " " + hand.pointables[1].tipPosition[2];
          // console.log(direction)
          // CURSOR.setAttribute("rotation", direction);
          //INDEX FINGER
          var dotIndexFingerProximalMedial = Leap.vec3.dot(
            hand.indexFinger.proximal.direction(),
            hand.indexFinger.medial.direction()
          );
          var dotIndexFingerMetacarpalProximal = Leap.vec3.dot(
            hand.indexFinger.metacarpal.direction(),
            hand.indexFinger.proximal.direction()
          );
          //----------------------------------------------------
          //MIDDLE FINGER
          var dotMiddleFingerProximalMedial = Leap.vec3.dot(
            hand.middleFinger.proximal.direction(),
            hand.middleFinger.medial.direction()
          );

          var dotMiddleFingerMetacarpalProximal = Leap.vec3.dot(
            hand.middleFinger.metacarpal.direction(),
            hand.middleFinger.proximal.direction()
          );
          //----------------------------------------------------
          //RING FINGER
          var dotRingFingerProximalMedial = Leap.vec3.dot(
            hand.ringFinger.proximal.direction(),
            hand.ringFinger.medial.direction()
          );

          var dotRingFingerMetacarpalProximal = Leap.vec3.dot(
            hand.ringFinger.metacarpal.direction(),
            hand.ringFinger.proximal.direction()
          );
          //----------------------------------------------------
          //RING FINGER
          var dotPinkyProximalMedial = Leap.vec3.dot(
            hand.pinky.proximal.direction(),
            hand.pinky.medial.direction()
          );

          var dotPinkyMetacarpalProximal = Leap.vec3.dot(
            hand.pinky.metacarpal.direction(),
            hand.pinky.proximal.direction()
          );
          //----------------------------------------------------

          //ANGLE INDEX FINGER
          var angleIndexFingerProximalMedial = Math.acos(
            dotIndexFingerProximalMedial
          );
          var angleIndexFingerMetacarpalProximal = Math.acos(
            dotIndexFingerMetacarpalProximal
          );
          //----------------------------------------------------
          // ANGLE MIDDLE FINGER
          var angleMiddleFingerProximalMedial = Math.acos(
            dotMiddleFingerProximalMedial
          );
          var angleMiddleFingerMetacarpalProximal = Math.acos(
            dotMiddleFingerMetacarpalProximal
          );
          //----------------------------------------------------
          // ANGLE RING FINGER
          var angleRingFingerProximalMedial = Math.acos(
            dotRingFingerProximalMedial
          );
          var angleRingFingerMetacarpalProximal = Math.acos(
            dotRingFingerMetacarpalProximal
          );
          //----------------------------------------------------
          // ANGLE PINKY FINGER
          var anglePinkyProximalMedial = Math.acos(dotPinkyProximalMedial);
          var anglePinkyMetacarpalProximal = Math.acos(
            dotPinkyMetacarpalProximal
          );
          if (
            angleIndexFingerMetacarpalProximal < 0.5 &&
            angleIndexFingerProximalMedial > 0.9 &&
            angleIndexFingerProximalMedial < 1.1 &&
            angleMiddleFingerMetacarpalProximal < 0.5 &&
            angleMiddleFingerProximalMedial > 0.9 &&
            angleMiddleFingerProximalMedial < 1.5 &&
            angleRingFingerMetacarpalProximal < 0.5 &&
            angleRingFingerProximalMedial > 0.75 &&
            angleRingFingerProximalMedial < 1.5 &&
            anglePinkyMetacarpalProximal < 0.5 &&
            anglePinkyProximalMedial > 0.75 &&
            anglePinkyProximalMedial < 1.5
          ) {
            console.log("C")
          } else if (
            !hand.indexFinger.extended &&
            !hand.middleFinger.extended &&
            !hand.ringFinger.extended &&
            !hand.pinky.extended &&
            hand.thumb.extended
          ) {
            console.log("A")
          } else if (
            hand.indexFinger.extended &&
            hand.middleFinger.extended &&
            hand.ringFinger.extended &&
            hand.pinky.extended &&
            !hand.thumb.extended
          ) {
            console.log("B")
          } else if (
            hand.indexFinger.extended &&
            !hand.middleFinger.extended &&
            !hand.ringFinger.extended &&
            !hand.pinky.extended &&
            !hand.thumb.extended
          ) {
            console.log("D")
          }
        }
      }
    }
  );